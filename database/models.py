from sqlalchemy import (Table, Column, ForeignKey,
                        Integer, String, Date, Boolean)
from sqlalchemy.orm import relationship

from .database import Base

like_table = Table('like', Base.metadata,
                   Column('user_id', Integer, ForeignKey('users.id')),
                   Column('image_id', Integer, ForeignKey('images.id'))
                   )


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String(255), unique=True, index=True)
    username = Column(String(32), unique=True, index=True)
    hashed_password = Column(String(255))
    join_date = Column(Date)
    avatar = Column(String(255))
    pix_points = Column(Integer)

    creations = relationship("Image", backref="owner")
    liked = relationship("Image",
                         secondary=like_table,
                         backref="likers")


class Image(Base):
    __tablename__ = "images"

    id = Column(Integer, primary_key=True, index=True)
    path = Column(String(255))
    title = Column(String(32))
    public = Column(Boolean)
    owner_id = Column(Integer, ForeignKey("users.id"))
