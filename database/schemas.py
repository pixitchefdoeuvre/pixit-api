from __future__ import annotations

from datetime import date, datetime, timedelta
from typing import List, Optional

from pydantic import BaseModel


###############
#    IMAGES   #
###############
class ImageBase(BaseModel):
    title: str = ""
    public: bool = False


class ImageCreate(ImageBase):
    data: str = ""


class ImageInDB(ImageBase):
    owner_id: int = 0
    path: str = ""


class Image(ImageInDB):
    id: int = 0

    likers: List[User] = []

    class Config:
        orm_mode = True


###############
#    USERS    #
###############
class UserBase(BaseModel):
    username: str
    join_date: date = datetime.now()
    pix_points: int = 0


class UserCreate(UserBase):
    email: str
    password: str


class UserProfilePublic(UserBase):
    id: int

    class Config:
        orm_mode = True


class UserProfilePrivate(UserProfilePublic):
    email: str


class User(UserProfilePrivate):
    creations: List[Image] = []
    liked: List[Image] = []


Image.update_forward_refs()


###############
#   SECURITY  #
###############

class Token(BaseModel):
    access_token: str
    ttl: timedelta
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None
