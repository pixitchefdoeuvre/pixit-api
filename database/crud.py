from passlib.context import CryptContext
from sqlalchemy import desc
from sqlalchemy.orm import Session
from sqlalchemy.sql import func

from database import models, schemas

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_password_hash(password):
    return pwd_context.hash(password)


#############
#   USERS   #
#############
def create_user(db: Session, user: schemas.UserCreate):
    """
    Sauvegarde un nouvel User en BDD
    """

    model = models.User(
        email=user.email,
        username=user.username,
        join_date=user.join_date,
        pix_points=user.pix_points,
        hashed_password=get_password_hash(user.password)
    )
    db.add(model)
    db.commit()
    db.refresh(model)  # lit l'id
    return model


def get_user(db: Session, user_id: int):
    """
    Renvoie l'User identifié par {user_id}
    """

    return db.query(models.User).filter(models.User.id == user_id).first()


def get_username(db: Session, user_id: int):
    """
    Renvoie le nom d'utilisateur de l'User identifié par {user_id}
    """
    # TODO traiter caractères spéciaux
    return (db.query(models.User.username)
            .filter(models.User.id == user_id).first()).username


def get_user_by_email(db: Session, email: str):
    """
    Renvoie l'User identifié par {email}
    """

    return (db.query(models.User).filter(models.User.email == email)
            .first())


def get_user_by_username(db: Session, username: str):
    """
    Renvoie l'User identifié par {username}
    """

    criterion = models.User.username == username
    return db.query(models.User).filter(criterion).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    """
    Renvoie les {limit} premiers Users stockés en BDD
    """

    return db.query(models.User).offset(skip), limit(limit).all()


def get_popular_users(db: Session, skip: int = 0, limit: int = 100):
    """
    Renvoie les noms des {limit} premiers Users stockés en BDD,
    classés selon leur popularité
    """

    return db.query(models.User.username).order_by(desc("pix_points"))\
        .offset(skip).limit(limit).all()


def update_pix_points(db: Session, user: models.User, new_pix_points: int):
    """
    Affecte la valeur {new_pix_points} aux pix points de {user}
    """

    user.pix_points = new_pix_points
    db.commit()
    return new_pix_points


##############
#   IMAGES   #
##############
async def create_image(db: Session, image: schemas.ImageInDB):
    """
    Sauvegarde une nouvelle Image en BDD
    """

    model = models.Image(**image.dict())
    db.add(model)
    db.commit()
    db.refresh(model)  # lit l'id
    return model


def get_image(db: Session, image_id: int):
    """
    Renvoie l'Image identifié par {image_id}
    """

    return (db.query(models.Image).filter(models.Image.id == image_id)
            .first())


def get_images(db: Session, skip: int = 0, limit: int = 100):
    """
    Renvoie les {limit} premières Images stockées en BDD
    """

    return db.query(models.images).offset(skip), limit(limit).all()


def get_public_recent_images(db: Session, skip: int = 0, limit: int = 100):
    """
    Renvoie les {limit} premières Images stockées en BDD,
    dont l'attribut public = true
    """

    return (db.query(models.Image).filter_by(public=True).order_by(desc("id"))
            .offset(skip).limit(limit).all())


def get_public_popular_images(db: Session, skip: int = 0, limit: int = 100):
    """
    Renvoie les {limit} premières Images stockées en BDD,
    dont l'attribut public = true
    """

    # récupère le nombre de likes par image
    like_query = (db.query(models.like_table.c.image_id,
                           func.count(models.like_table.c.user_id).
                           label("like_count"))
                  .group_by(models.like_table.c.image_id).subquery())
    # jointure avec les images et tri
    return (db.query(models.Image)
            .outerjoin(like_query, models.Image.id == like_query.c.image_id)
            .filter(models.Image.public)
            .order_by(desc(like_query.c.like_count))
            .offset(skip).limit(limit).all())


def get_images_by_owner(db: Session, owner: int):
    """
    Renvoie les Images dont l'auteur a pour id {owner}
    """

    return (db.query(models.Image).filter_by(owner_id=owner)
            .order_by(desc("id")).all())


#############
#   LIKES   #
#############
def get_user_has_liked(db: Session, user_id: int, image_id: int):
    """
    Renvoie true si l'utilisateur identifié par {user_id}
    a liké l'image identifiée par {image_id}
    """

    return (db.query(models.like_table)
            .filter_by(user_id=user_id, image_id=image_id).count()) == 1


def create_like(db: Session, user: models.User, image: models.Image):
    """
    Enregistre un Like en BDD, de la part de {user} vers {image}
    """

    user.liked.append(image)
    db.commit()


def delete_like(db: Session, user: models.User, image: models.Image):
    """
    Enlève un Like en BDD, de la part de {user} vers {image}
    """

    user.liked.remove(image)
    db.commit()


def get_user_images_liked(db: Session, user_id: int):
    """
    Renvoie les Images likées par l'utilisateur d'id {user_id}
    """

    user = get_user(db, user_id)
    return user.liked
