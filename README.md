# API pour l'application Pixit

## Application de création et de partage de logos

Installer FastAPI :
> pip install fastapi

Installer SQLAlchemy (BDD & ORM) :
> pip install SQLAlchemy

Installer Uvicorn (serveur) :
> pip install "uvicorn[standard]"

Lancer le serveur :
> uvicorn main:app --reload

Le serveur se trouve à l'adresse
> http://127.0.0.1:8000/