from os import path

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from database import schemas, crud
from dependencies import get_current_user, get_db, clean_filename

router = APIRouter(
    prefix="/images",
    tags=["images"]
)

FOLDER = "static/"


@router.post("/")
async def post_new_image(image: schemas.ImageCreate,
                         user: schemas.User = Depends(get_current_user),
                         db: Session = Depends(get_db)):
    # vérifie si le nom de fichier est déjà utilisé
    file_path = clean_filename(image.title + ".svg")
    if path.isfile(FOLDER + file_path):
        suffix = 1
        while path.isfile(FOLDER + file_path):
            file_path = clean_filename(image.title + str(suffix) + ".svg")
            suffix = suffix + 1

    with open(FOLDER + file_path, "w") as f:
        f.write(image.data)

    # tronque le titre si nécessaire
    if len(image.title) > 32:
        image.title = image.title[:32]

    # sauvegarde l'image en BDD
    imageDB = schemas.ImageInDB(
        **image.dict(), path=file_path, owner_id=user.id)
    imageDB = await crud.create_image(db, imageDB)

    return {"path": file_path, "image": imageDB}


@router.get("/public/popular/{skip}/{limit}")
def get_public_popular_images(skip: int,
                              limit: int,
                              db: Session = Depends(get_db)):
    return crud.get_public_popular_images(db, skip, limit)


@router.get("/public/recent/{skip}/{limit}")
def get_public_recent_images(skip: int,
                             limit: int,
                             db: Session = Depends(get_db)):
    return crud.get_public_recent_images(db, skip, limit)


@router.get("/public")
def get_public_images(db: Session = Depends(get_db)):
    return crud.get_public_recent_images(db)


@router.get("/owner/{owner_id}", tags=["users"])
def get_user_images(owner_id: int, db: Session = Depends(get_db)):
    return crud.get_images_by_owner(db, owner_id)
