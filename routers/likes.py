from fastapi import APIRouter, Depends, status
from sqlalchemy.orm import Session

from database import crud
from database.models import User
from dependencies import get_current_user, get_db

router = APIRouter(
    prefix="/likes",
    tags=["likes"]
)

LIKE_PIX_POINTS = 5


@router.get("/{image_id}")
def current_user_has_liked(image_id: int,
                           user: User = Depends(get_current_user),
                           db: Session = Depends(get_db)):
    return crud.get_user_has_liked(db, user.id, image_id)


@router.post("/{image_id}", status_code=status.HTTP_201_CREATED)
def current_user_add_like(image_id: int,
                          user: User = Depends(get_current_user),
                          db: Session = Depends(get_db)):
    image = crud.get_image(db, image_id)
    crud.create_like(db, user, image)


@router.delete("/{image_id}")
def current_user_delete_like(image_id: int,
                             user: User = Depends(get_current_user),
                             db: Session = Depends(get_db)):
    image = crud.get_image(db, image_id)
    crud.delete_like(db, user, image)


@router.get("/user/{user_id}")
def get_user_images_liked(user_id: int, db: Session = Depends(get_db)):
    return crud.get_user_images_liked(db, user_id)
