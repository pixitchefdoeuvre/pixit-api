from datetime import timedelta

from fastapi import APIRouter, HTTPException, Depends, status
from fastapi.security import OAuth2PasswordRequestFormStrict
from sqlalchemy.orm import Session

from database import crud
from database.schemas import Token, User, UserCreate
from dependencies import get_db, authenticate_user, create_access_token

ACCESS_TOKEN_EXPIRE_MINUTES = 30

router = APIRouter(tags=["security"], prefix="/auth")


@router.post("/signup", response_model=User, tags=["users"])
def create_user(user: UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email déjà utilisé")
    db_user = crud.get_user_by_username(db, username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="Pseudonyme déjà utilisé")
    return crud.create_user(db=db, user=user)


@router.post("/signin", response_model=Token)
async def login_for_access_token(
        db: Session = Depends(get_db),
        form_data: OAuth2PasswordRequestFormStrict = Depends()
):
    # récupération des données
    user = authenticate_user(db, form_data.username, form_data.password)
    # vérification
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Nom d'utilisateur ou mot de passe incorrect",
            headers={"WWW-Authenticate": "Bearer"}
        )
    # création du token
    ttl = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username},
        expires_delta=ttl
    )

    return {"access_token": access_token,
            "token_type": "bearer",
            "ttl": ttl}
