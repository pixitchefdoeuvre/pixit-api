from urllib.parse import unquote

from fastapi import APIRouter, Depends, Body
from sqlalchemy.orm import Session

from database import schemas, crud
from dependencies import get_current_user, get_db

router = APIRouter(
    prefix="/users",
    tags=["users"]
)


@router.get("/me", response_model=schemas.UserProfilePrivate)
def read_current_user(user: schemas.User = Depends(get_current_user)):
    return user


@router.get("/name/{user_id}")
def get_username(user_id: int, db: Session = Depends(get_db)):
    return crud.get_username(db, user_id)


@router.patch("/{user_id}/pixpoints/add")
def modify_pix_points_user(user_id: int,
                           pix_points: int = Body(
                               ...,
                               description="""Le nombre de PixPoints
                            à ajouter (ou enlever)"""),
                           db: Session = Depends(get_db)):
    user = crud.get_user(db, user_id)
    pix_points = max(user.pix_points + pix_points, 0)
    pix_points = crud.update_pix_points(
        db, user=user, new_pix_points=pix_points)
    return pix_points


@router.get("/{user_id}")
def get_user(user_id: int, db: Session = Depends(get_db)):
    return crud.get_user(db, user_id)


@router.get("/popular/{skip}/{limit}")
def get_popular_users(skip: int,
                      limit: int,
                      db: Session = Depends(get_db)):
    return crud.get_popular_users(db, skip, limit)


@router.get("/", response_model=schemas.UserProfilePublic)
def get_user_by_username(username: str, db: Session = Depends(get_db)):
    user = crud.get_user_by_username(db, unquote(username))
    return user
    # return unquote(username)
