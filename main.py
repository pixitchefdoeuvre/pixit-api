from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles

from database import models
from database.database import engine
from routers import users, images, security, likes

app = FastAPI()

###############
#   CORS  #
###############
origins = r"^http://(localhost|127\.0\.0\.1):808\d"

app.add_middleware(
    CORSMiddleware,
    allow_origin_regex=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

###############
#   DATABASE  #
###############
models.Base.metadata.create_all(bind=engine)

###############
#   ROUTING  #
###############
app.include_router(users.router, prefix="/api")
app.include_router(images.router, prefix="/api")
app.include_router(likes.router, prefix="/api")
app.include_router(security.router, prefix="/api")

################
# STATIC FILES #
################
app.mount("/static", StaticFiles(directory="static"), name="static")


# Docs
@app.get("/")
def root():
    return RedirectResponse("/docs")
